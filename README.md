# Пример описания API в спецификации openapi 3

Пример удовлетворяет требованиям
- [Ensi Api Design Guide](https://docs.google.com/document/d/1Sj-G3lWzJvXmeojRv8yQb3ZSZeL3n6fa8EKN12kLvgg/edit?usp=sharing)
- [laravel-openapi-client-generator](https://github.com/greensight/laravel-openapi-client-generator/blob/master/docs/api_schema_requirements.md)
- [laravel-openapi-server-generator](https://github.com/greensight/laravel-openapi-server-generator/blob/master/docs/api_schema_requirements.md)

Название директории `customers` соответствует названию модуля API в вашем сервисе. 
Если ваш сервис маленький и не использует модулей, то этот уровень вложенности следует опустить.

Для лучшего понимании иерархии схем для ресурса полезна [визуализация в Miro](https://miro.com/app/board/o9J_lPKk7yY=/) 

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
